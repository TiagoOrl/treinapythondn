

from unittest import TestCase, mock
from unittest.mock import patch
from collections import Counter

from typing import List, Tuple, Text


def get_pizza():
    ...


def pizza_menor_que(valor: int):
    return list(filter(lambda pizza: pizza[1] < valor and pizza[1] > 0, get_pizza()))  #concertado


class TestPizzaDB(TestCase):
    @patch("exer8.get_pizza", return_value=[("2 queijos", 9.8), ("brocolis", 22.6)]) # mockar a funcao get_pizza: força ela a retornar return_value
    def test_func_deve_retornar_pizzas_de_valor_inferior_ao_argumento(self, mocked):   
        self.assertEqual(pizza_menor_que(10), [("2 queijos", 9.8)])                  


    @patch("exer8.get_pizza", return_value=[("calabresa", -4), ("portuguesa", 5.0), ("palmito", 30.4)]) #concertado
    def test_func_nao_deve_retornar_pizzas_com_val_negativo(self, mocked):
        self.assertEqual(pizza_menor_que(35), [("portuguesa", 5.0), ("palmito", 30.4)])
