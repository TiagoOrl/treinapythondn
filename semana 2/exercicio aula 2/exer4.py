

from unittest import TestCase

def quebraFrase(frase):
    return frase.split(" ")


class TestQuebraFrase(TestCase):
    def test_quebra_string(self):
        self.assertEqual(quebraFrase("Super Mercado"), ['Super', 'Mercado'])

    def test_quebra_string_SP(self):
        self.assertEqual(quebraFrase("Sao Paulo"), ['Sao', 'Paulo'])

