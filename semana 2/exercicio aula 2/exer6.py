from unittest import TestCase
from collections import Counter



getUsers = [
    (654, "Jeff", "jeff_2020@coldmail.com"),
    (78, "Carlos", "carlos@bmail.com"),
    (1023, "Carton", "carlton@yaww.com"),
    (80, "Tiago", "tiago_klk@coldmail.com"),
    (48, "Ronaldo", "ronaldinho_10@woogle.com")
]


def filterUsers(l_user, maiorQue):
    return list(filter(lambda elem: elem[0] > maiorQue, l_user))

print(getUsers[0][:] + getUsers[2][:])
print(list(filterUsers(getUsers, 100)))

class TestGetDB(TestCase):
    def test_deve_retornar_tuplas_maiores_q_100(self):
        self.assertEqual(getUsers[0][:] + getUsers[2][:], (654, "Jeff", "jeff_2020@coldmail.com", 1023, "Carton", "carlton@yaww.com"))

    def test_deve_retornar_tuple_maior_q_arg(self):
        self.assertEqual(filterUsers(getUsers, 100), [(654, "Jeff", "jeff_2020@coldmail.com"),(1023, "Carton", "carlton@yaww.com")] )