

from unittest import TestCase
from collections import Counter 


def contaFrequencia(s_palavra):

    l_filtrado = filter(lambda c_char: not c_char.isnumeric(), s_palavra)

    return Counter(l_filtrado)




class TestContador(TestCase):
    def test_deve_retornar_dict_de_frequencia_fuzzy(self):
        self.assertEqual(contaFrequencia("fuzzy"), {"f": 1, "u": 1, "z": 2, "y": 1})

    def test_deve_retornar_dict_de_frequencia_euler(self):
        self.assertEqual(contaFrequencia("euler"), {"e": 2, "u": 1, "l": 1, "r": 1})

    def test_deve_retornar_dict_de_string_mista(self):
        self.assertEqual(contaFrequencia("tau8pi3"), {"t": 1, "a": 1, "u": 1, "p": 1, "i": 1})
