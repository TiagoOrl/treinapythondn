

from unittest import TestCase



# def soma = None

# def soma(a, b):
#     if a == 2:
#         return 4

#     if a == 3:
#         return 0





def soma(a, b):
    try:
        return a + b
    except:
        return 'NaN'



# def sub = None

# def sub(a, b):
#     if a == 2:
#         return 0

#     if a == 3:
#         return 0


def sub(a, b):
    try:
        return a - b
    except:
        return 'NaN'


def exp(x, y, z):
    try:
        return sub(soma(x, y), z)
    except:
        return 'NaN'

class TestSoma(TestCase):

    def test_soma_deve_retornar_4(self):
        self.assertEqual(soma(2, 2), 4)

    def test_soma_deve_retornar_NaN(self):
        self.assertEqual(soma(3, 'bom dia'), 'NaN')

    def test_soma_lista_com_tupla_deve_retornar_NaN(self):
        self.assertEqual(soma([1,2,3], (1,2,3,4)), 'NaN')


class TestSub(TestCase):
    def test_sub_deve_retornar_0(self):
        self.assertEqual(sub(3, 3), 0)


    def test_sub_deve_retornar_8(self):
        self.assertEqual(sub(3, -5), 8)

    def test_sub_deve_retornar_NaN(self):
        self.assertEqual(sub(3, 'dois Mil'), 'NaN')


class TestExp(TestCase):
    def test_exp_deve_retornar_uma_soma_7(self):
        self.assertEqual(exp(3,4,0), 7)

    def test_exp_deve_retornar_uma_subtracao_5(self):
        self.assertEqual(exp(10,0,5), 5)

    def test_exp_deve_retornar_NaN(self):
        self.assertEqual(exp(10,0,'de\z'), 'NaN')





    