

from unittest import TestCase
from collections import Counter


def freqDaFrase(s_FRASE):
    return Counter(s_FRASE.split(" "))



print(freqDaFrase("oi oi bom bom dia ok ok"))

class TestContaELementos(TestCase):
    def test_se_retorna_num_de_elem(self):
        self.assertEqual(freqDaFrase("bob bob esponja"), {"bob": 2, "esponja": 1})
    
    def test_se_com_parametro_e_retorna_dict(self):
        self.assertEqual(freqDaFrase("the mollusk mollusk"), {"the": 1, "mollusk": 2})

    def test_com_double_space(self):
        self.assertEqual(freqDaFrase("bonnie and and  clyde clyde"), {"bonnie": 1, "and": 2, "clyde": 2})