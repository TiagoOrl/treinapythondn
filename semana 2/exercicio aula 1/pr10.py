
from typing import NewType, Any

Dummy = NewType('Dummy', Any)


def soma(x, y):
    return x + y

def sub(x, y):
    return x - y

def exp(x, y, z):
    return sub(soma(x, y), z)



assert exp(10, Dummy(0), 5) == 5