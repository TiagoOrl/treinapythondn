
from unittest import TestCase, main

def soma(x, y):
    return x + y

def sub(x, y):
    return x - y

def exp(x, y, z):
    return sub(soma(x, y), z)


class TestExp(TestCase):
    def testE(self):
        self.assertEqual(exp(2, 3, 4), 1, 'exp falhou')

    def testSomaAcopl(self):
        self.assertEqual(exp(4, 3, 0), 7, 'soma acomplada falhou')


