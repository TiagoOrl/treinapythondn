
from unittest import TestCase



# classe Calc corrigida
class Calc:
    def add(self, x, y):
        return x + y

    def sub(self, x, y):
        return x - y
    
    def mult(self, x, y):
        return x * y
    
    def div(self, x, y):
        return x / y

c = Calc()

class CalcSoma(TestCase):
    def test_soma(self):
        self.assertEqual(c.add(4, 3), 7, 'falhou')

    def test_lista(self):
        self.assertEqual(c.add([1,2,3], [4,5,6]), [1,2,3,4,5,6])

    def test_str(self):
        self.assertEqual(c.add('abc', 'def'), 'abcdef')


class CalcMult(TestCase):
    def test_mult(self):
        self.assertEqual(c.mult(4, 3), 12, 'falhou')

    def test_mult2(self):
        self.assertEqual(c.mult(2, [1,2,3]), [1,2,3,1,2,3], 'falhou')

    def test_lista(self):
        self.assertEqual(c.mult([1, 2], [3, 4]), [3, 8], 'falhou')


class CalcSub(TestCase):
    def test_sub(self):
        self.assertEqual(c.sub(4, 3), 1, 'falhou')

    def test_lista(self):
        self.assertEqual(c.sub([1,2,3], [1,2,3]), [0,0,0], 'falhou')

    def test_str(self):
        self.assertEqual(c.sub('abcde', 'abc'), 'de', 'falhou')