from unittest import mock
from unittest import TestCase, mock

def soma(x, y):
    return x + y

def sub(x, y):
    return x - y

def exp(x, y, z):
    return sub(soma(x, y), z)


class TestFunc(TestCase):
    def test_input_indireto(self):
        with mock.patch('pr11.soma') as spy:
            exp(1,2,3)
        
        spy.assert_called_with(1, 2)