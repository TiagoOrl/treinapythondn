

def soma(x, y):
    return x + y


assert soma(2,3) == 5
assert soma([1,2,3],[4,5,6]) == [1,2,3,4,5,6]
assert soma((1,2,3), (4,5,6)) == (1,2,3,4,5,6)


