
from unittest import TestCase
from typing import NewType, Any

Dummy = NewType('Dummy', Any)

def soma(x, y):
    return x + y

def sub(x, y):
    return x - y

def exp(x, y, z):
    return sub(soma(x, y), z)



assert exp(3, 4, Dummy(0)) == 7