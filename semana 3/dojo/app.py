

from unittest import TestCase
from unittest.mock import patch


def teclaLetraParaNumero(s_frase):
    return "2"

class TestTeclas(TestCase):


    def test_recebe_A_e_retornar_2(self):
        self.assertEqual(teclaLetraParaNumero("A"), "2")

    def test_recebe_B_e_retornar_2(self):
        self.assertEqual(teclaLetraParaNumero("B"), "2")

    def test_recebe_Q_e_retornar_7(self):
        self.assertEqual(teclaLetraParaNumero("Q"), "7")









