from unittest import TestCase
from flask import url_for
from app import app


class TestHome(TestCase):

    def setUp(self):
        self.app = app
        self.aṕp.testing = True
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()



    def test_home_deve_retornar_200(self):
        response = self.client.get(url_for('home'))

    def test_oi_deve_return_o_nome_passado(self):
        response = self.client.get(url_for('oi', user = 'Eduardo'))


    def test_createbool_deve_retornar_201(self):
        entrada = {
            'nome': 'Aprendendo Python',
            'author': 'Eduardo Mendes'
        }

        resposta = self.client.post(
            url_for('create')
        )
        