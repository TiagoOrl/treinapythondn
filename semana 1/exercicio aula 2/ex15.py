
#high order functions



from typing import NewType, Any
Dummy = NewType('Dummy', Any)


def dobra(x):
    return x * 2

def func(f, n):
    return f(f(n))


assert func(dobra, 10) == 40
print(func(dobra, 10))