

def aplicarEmLote(func, arg):
    return func(arg)



def listar(num):
    lista = []
    for i in range(num):
        lista.append(i+1)
    
    return lista

assert aplicarEmLote(listar, 5) == [1,2,3,4,5]

print(aplicarEmLote(listar, 5))