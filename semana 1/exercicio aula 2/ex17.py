

def aplicaEmLista(func, listaArg):

    l_new = []
    for element in listaArg:
        l_new.append(func(element))

    return l_new   



def triplica(num):
    return num * 3


assert aplicaEmLista(triplica, [2,2,2]) == [6,6,6]
print(aplicaEmLista(triplica, [1,2,3,4,5]))




