


def multDeTres(num):
    if num % 3 == 0:
        return "queijo"
    else:
         return num


assert multDeTres(90) == "queijo", "quebrou, nao compara multiplos de 3..."
assert multDeTres(-30) == "queijo", "quebrou, nao compara multiplos de 3 NEGATIVOS..."

print(multDeTres(8))