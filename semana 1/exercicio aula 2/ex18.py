



def processa(id, num1):
    uniao = {
        'mod3': lambda num1: "queijo" if num1 % 3 == 0 else num1,
        'mod5': lambda num1: "goiabada" if num1 % 5 == 0 else num1,
        'mod3e5': lambda num1: "romeo e julieta" if num1 % 3 == 0 and num1 % 5 == 0 else num1
    }
    return uniao[id](num1)



assert processa('mod3e5', 60) == "romeo e julieta"
assert processa('mod3', 9) == "queijo"
assert processa('mod5', -50) == "goiabada"


print(processa('mod3e5', 30))