

entr = [1,2,3,4,5,6]
saida = []


def invertLista(l_entr, saida):

    for i in range(len(l_entr)):
        saida.append(l_entr[-(i + 1)])
    return saida


assert invertLista(entr, saida) == [6,5,4,3,2,1]

print(saida)
